package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.junit.AbstractTest;
import de.maltebehrendt.uppt.util.EncryptionUtils;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.ext.unit.Async;
import io.vertx.ext.unit.TestContext;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import java.lang.invoke.MethodHandles;
import java.util.UUID;

public class UserAuthService1Test extends AbstractTest {
    // TODO: test what happens if user removed in meantime and session start/end
    // TODO: test invalid requests, security violations, ...

    @BeforeClass
    public static void setup(TestContext context) {
        setupTestVertxAndDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @AfterClass
    public static void teardown(TestContext context) {
        //tearDownTestVertxAndCleanupDatabase(MethodHandles.lookup().lookupClass().getSimpleName(), context);
    }

    @Test
    public void testProcessUserRegisteredWithoutRolesMinimum(TestContext context) {
        Async testResult = context.async(1);

        String emailAddress = getRandomMailAddress();

        JsonObject authMethodConfig = new JsonObject()
                .put("type", "email")
                .put("emailAddress", emailAddress)
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String passwordHash = EncryptionUtils.hashForKey("someTestPW", authMethodConfig.getString("hashFactory"),authMethodConfig.getString("hashSalt"), authMethodConfig.getInteger("hashIterations"), authMethodConfig.getInteger("hashLength"));
        authMethodConfig.put("passwordHash", passwordHash);

        send("userAuth", "1", "userRegistered", new JsonObject()
                        .put("authMethodConfig", authMethodConfig)
                        .put("solutionAddresses", new JsonArray().add(new JsonObject().put("domain", "test").put("version", "1").put("type", "dummy")))
                , reply -> {
                    context.assertTrue(reply.succeeded());
                    context.assertEquals(200, reply.statusCode());

                    JsonObject result = reply.getBodyAsJsonObject();
                    context.assertNotNull(result.getString("userID"));
                    context.assertEquals("active", result.getString("status"));
                    context.assertEquals(0, result.getJsonArray("userRoles").size());
                    testResult.countDown();
                });

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessUserRegisteredWithRolesMinimum(TestContext context) {
        Async testResult = context.async(1);

        String emailAddress = getRandomMailAddress();

        JsonObject authMethodConfig = new JsonObject()
                .put("type", "email")
                .put("emailAddress", emailAddress)
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String passwordHash = EncryptionUtils.hashForKey("someTestPW", authMethodConfig.getString("hashFactory"),authMethodConfig.getString("hashSalt"), authMethodConfig.getInteger("hashIterations"), authMethodConfig.getInteger("hashLength"));
        authMethodConfig.put("passwordHash", passwordHash);

        send("userAuth", "1", "userRegistered", new JsonObject()
                        .put("authMethodConfig", authMethodConfig)
                        .put("solutionAddresses", new JsonArray().add(new JsonObject().put("domain", "test").put("version", "1").put("type", "dummy")))
                        .put("userRoles", new JsonArray().add("testUser").add("tester"))
                , reply -> {
                    context.assertTrue(reply.succeeded());
                    context.assertEquals(200, reply.statusCode());

                    JsonObject result = reply.getBodyAsJsonObject();
                    context.assertNotNull(result.getString("userID"));
                    context.assertEquals("active", result.getString("status"));
                    context.assertEquals(2, result.getJsonArray("userRoles").size());
                    context.assertTrue(result.getJsonArray("userRoles").contains("testUser"));
                    context.assertTrue(result.getJsonArray("userRoles").contains("tester"));

                    testResult.countDown();
                });

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessAuthMethodConfigMissingForEmail(TestContext context) {
        Async testResult = context.async(1);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertFalse(registrationResult.failed());

            JsonObject user = registrationResult.result();

            send("userAuth", "1", "authMethodConfigMissing", new JsonObject()
                            .put("authMethodIdentifier", new JsonObject()
                                .put("type", "email")
                                .put("emailAddress", user.getString("emailAddress")))
                    , reply -> {
                        context.assertTrue(reply.succeeded());
                        context.assertEquals(200, reply.statusCode());

                        JsonObject result = reply.getBodyAsJsonObject();
                        context.assertTrue(result.containsKey("authMethodConfig"));

                        JsonObject authMethodConfig = result.getJsonObject("authMethodConfig");
                        context.assertFalse(authMethodConfig.isEmpty());
                        context.assertNotNull(authMethodConfig.getString("hashFactory"));
                        context.assertNotNull(authMethodConfig.getString("hashSalt"));
                        context.assertNotNull(authMethodConfig.getInteger("hashIterations"));
                        context.assertNotNull(authMethodConfig.getInteger("hashLength"));
                        context.assertEquals("unverified", authMethodConfig.getString("status"));

                        testResult.countDown();
                    });

        });
        getNewUser(null, userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessAuthMethodConfigMissingForUnknownEmail(TestContext context) {
        Async testResult = context.async(1);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertFalse(registrationResult.failed());

            JsonObject user = registrationResult.result();

            send("userAuth", "1", "authMethodConfigMissing", new JsonObject()
                            .put("authMethodIdentifier", new JsonObject()
                                    .put("type", "email")
                                    .put("emailAddress", user.getString("emailAddress")+1))
                    , reply -> {
                        context.assertTrue(reply.succeeded());
                        context.assertEquals(404, reply.statusCode());

                        testResult.countDown();
                    });

        });
        getNewUser(null, userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionRegistrationMissingAnonymousMinimum(TestContext context) {
        Async testResult = context.async(1);

        JsonObject sessionTokenHashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", sessionTokenHashConfig)
                , reply -> {
                    context.assertTrue(reply.succeeded());
                    context.assertEquals(200, reply.statusCode());

                    testResult.countDown();
                });

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionAuthenticationMissingWrongPW(TestContext context) {
        Async testResult = context.async(2);

        JsonObject hashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", hashConfig)
                , sessionReply -> {
                    context.assertTrue(sessionReply.succeeded());
                    context.assertEquals(200, sessionReply.statusCode());
                    testResult.countDown();

                    Future<JsonObject> userRegistrationFuture = Future.future();
                    userRegistrationFuture.setHandler(registrationResult -> {
                        context.assertTrue(registrationResult.succeeded());
                        JsonObject user = registrationResult.result();

                        send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", hashConfig
                                                .put("type", "email")
                                                .put("emailAddress", user.getString("emailAddress"))
                                                .put("passwordHash", user.getString("passwordHash")+2))
                                , authReply -> {
                                    context.assertTrue(authReply.succeeded());
                                    context.assertEquals(401, authReply.statusCode());
                                    testResult.countDown();
                                });
                    });
                    getNewUser(null, userRegistrationFuture);
                });

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionAuthenticationMissingWrongMail(TestContext context) {
        Async testResult = context.async(2);

        JsonObject hashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", hashConfig)
                , sessionReply -> {
                    context.assertTrue(sessionReply.succeeded());
                    context.assertEquals(200, sessionReply.statusCode());
                    testResult.countDown();

                    Future<JsonObject> userRegistrationFuture = Future.future();
                    userRegistrationFuture.setHandler(registrationResult -> {
                        context.assertTrue(registrationResult.succeeded());
                        JsonObject user = registrationResult.result();

                        send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", hashConfig
                                                .put("type", "email")
                                                .put("emailAddress", getRandomMailAddress())
                                                .put("passwordHash", user.getString("passwordHash")))
                                , authReply -> {
                                    context.assertTrue(authReply.succeeded());
                                    context.assertEquals(401, authReply.statusCode());
                                    testResult.countDown();
                                });
                    });
                    getNewUser(null, userRegistrationFuture);
                });

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionAuthenticationMissingNoUserRoles(TestContext context) {
        Async testResult = context.async(2);

        JsonObject hashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", hashConfig)
                , sessionReply -> {
                    context.assertTrue(sessionReply.succeeded());
                    context.assertEquals(200, sessionReply.statusCode());
                    testResult.countDown();

                    Future<JsonObject> userRegistrationFuture = Future.future();
                    userRegistrationFuture.setHandler(registrationResult -> {
                        context.assertTrue(registrationResult.succeeded());
                        JsonObject user = registrationResult.result();

                        send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", hashConfig
                                                .put("type", "email")
                                                .put("emailAddress", user.getString("emailAddress"))
                                                .put("passwordHash", user.getString("passwordHash")))
                                , authReply -> {
                            context.assertTrue(authReply.succeeded());
                            context.assertEquals(200, authReply.statusCode());

                            JsonObject result = authReply.getBodyAsJsonObject();
                            context.assertNotNull(result.getString("userID"));
                            context.assertEquals(user.getString("userID"), result.getString("userID"));
                            context.assertNotNull(result.getJsonArray("userRoles"));
                            context.assertEquals(0, result.getJsonArray("userRoles").size());
                            testResult.countDown();
                        });
                    });
                    getNewUser(null, userRegistrationFuture);
                });

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionAuthenticationMissingWithUserRoles(TestContext context) {
        Async testResult = context.async(2);

        JsonObject hashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                        .put("sessionTokenHash", token)
                        .put("sessionTokenHashConfig", hashConfig)
                , sessionReply -> {
                    context.assertTrue(sessionReply.succeeded());
                    context.assertEquals(200, sessionReply.statusCode());
                    testResult.countDown();

                    Future<JsonObject> userRegistrationFuture = Future.future();
                    userRegistrationFuture.setHandler(registrationResult -> {
                        context.assertTrue(registrationResult.succeeded());
                        JsonObject user = registrationResult.result();

                        send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", hashConfig
                                                .put("type", "email")
                                                .put("emailAddress", user.getString("emailAddress"))
                                                .put("passwordHash", user.getString("passwordHash")))
                                , authReply -> {
                                    context.assertTrue(authReply.succeeded());
                                    context.assertEquals(200, authReply.statusCode());

                                    JsonObject result = authReply.getBodyAsJsonObject();
                                    context.assertNotNull(result.getString("userID"));
                                    context.assertEquals(user.getString("userID"), result.getString("userID"));
                                    context.assertNotNull(result.getJsonArray("userRoles"));
                                    context.assertEquals(2, result.getJsonArray("userRoles").size());
                                    context.assertTrue(result.getJsonArray("userRoles").contains("tester"));
                                    context.assertTrue(result.getJsonArray("userRoles").contains("testAdmin"));
                                    testResult.countDown();
                                });
                    });
                    getNewUser(new JsonArray().add("tester").add("testAdmin"), userRegistrationFuture);
                });

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionAuthenticationMissingWithUserRolesNoSession(TestContext context) {
        Async testResult = context.async(1);

        JsonObject hashConfig = new JsonObject()
                .put("hashFactory", "PBKDF2WithHmacSHA512")
                .put("hashSalt", "testSalt")
                .put("hashIterations", 10)
                .put("hashLength", 256);

        String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
        // skipping calculating a hash here

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertTrue(registrationResult.succeeded());
            JsonObject user = registrationResult.result();

            send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                            .put("sessionTokenHash", token)
                            .put("authMethodConfig", hashConfig
                                    .put("type", "email")
                                    .put("emailAddress", user.getString("emailAddress"))
                                    .put("passwordHash", user.getString("passwordHash")))
                    , authReply -> {
                        context.assertTrue(authReply.succeeded());
                        context.assertEquals(404, authReply.statusCode());
                        testResult.countDown();
                    });
        });
        getNewUser(new JsonArray().add("tester").add("testAdmin"), userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionEnded(TestContext context) {
        Async testResult = context.async(1);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
                    context.assertTrue(registrationResult.succeeded());
                    JsonObject user = registrationResult.result();

                    send("userAuth", "1", "sessionEnded", new JsonObject()
                                    .put("sessionTokenHash", user.getString("sessionTokenHash"))
                            , endReply -> {
                                context.assertTrue(endReply.succeeded());
                                context.assertEquals(200, endReply.statusCode());
                                context.assertNotNull(endReply.getBodyAsJsonObject().getLong("endTimestamp"));
                                testResult.countDown();
                            });
                });
        getNewUserAndAuthenticatedSession(new JsonArray().add("tester"), userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionEndedInvalidSession(TestContext context) {
        Async testResult = context.async(1);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertTrue(registrationResult.succeeded());
            JsonObject user = registrationResult.result();

            send("userAuth", "1", "sessionEnded", new JsonObject()
                            .put("sessionTokenHash", user.getString("sessionTokenHash")+"ds")
                    , endReply -> {
                        context.assertTrue(endReply.succeeded());
                        context.assertEquals(404, endReply.statusCode());
                        testResult.countDown();
                    });
        });
        getNewUserAndAuthenticatedSession(new JsonArray().add("tester"), userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionEndedIfSessionAlreadyClosed(TestContext context) {
        Async testResult = context.async(2);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertTrue(registrationResult.succeeded());
            JsonObject user = registrationResult.result();

            send("userAuth", "1", "sessionEnded", new JsonObject()
                            .put("sessionTokenHash", user.getString("sessionTokenHash"))
                    , endReply -> {
                        context.assertTrue(endReply.succeeded());
                        context.assertEquals(200, endReply.statusCode());
                        context.assertNotNull(endReply.getBodyAsJsonObject().getLong("endTimestamp"));
                        testResult.countDown();

                        send("userAuth", "1", "sessionEnded", new JsonObject()
                                        .put("sessionTokenHash", user.getString("sessionTokenHash"))
                                , endReply2 -> {
                                    context.assertTrue(endReply2.succeeded());
                                    context.assertEquals(404, endReply2.statusCode());
                                    testResult.countDown();
                                });
                    });
        });
        getNewUserAndAuthenticatedSession(new JsonArray().add("tester"), userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionInterrupted(TestContext context) {
        Async testResult = context.async(1);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertTrue(registrationResult.succeeded());
            JsonObject user = registrationResult.result();

            send("userAuth", "1", "sessionInterrupted", new JsonObject()
                            .put("sessionTokenHash", user.getString("sessionTokenHash"))
                    , interruptionReply -> {
                        context.assertTrue(interruptionReply.succeeded());
                        context.assertEquals(200, interruptionReply.statusCode());
                        context.assertNotNull(interruptionReply.getBodyAsJsonObject().getLong("interruptedTimestamp"));
                        testResult.countDown();
                    });
        });
        getNewUserAndAuthenticatedSession(new JsonArray().add("tester"), userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionInterruptedInvalidSession(TestContext context) {
        Async testResult = context.async(1);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertTrue(registrationResult.succeeded());
            JsonObject user = registrationResult.result();

            send("userAuth", "1", "sessionInterrupted", new JsonObject()
                            .put("sessionTokenHash", user.getString("sessionTokenHash")+"ef")
                    , interruptionReply -> {
                        context.assertTrue(interruptionReply.succeeded());
                        context.assertEquals(404, interruptionReply.statusCode());
                        testResult.countDown();
                    });
        });
        getNewUserAndAuthenticatedSession(new JsonArray().add("tester"), userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcesSessionAuthenticationMissingViaSessionTokenButSessionNotInterrupted(TestContext context) {
        Async testResult = context.async(2);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertTrue(registrationResult.succeeded());
            JsonObject user = registrationResult.result();

            JsonObject sessionTokenHashConfig = new JsonObject()
                    .put("hashFactory", "PBKDF2WithHmacSHA512")
                    .put("hashSalt", "testSalt")
                    .put("hashIterations", 10)
                    .put("hashLength", 256);

            String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
            // skipping calculating a hash here

            send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                            .put("sessionTokenHash", token)
                            .put("sessionTokenHashConfig", sessionTokenHashConfig)
                    , reply -> {
                        context.assertTrue(reply.succeeded());
                        context.assertEquals(200, reply.statusCode());
                        testResult.countDown();

                        // authenticate this session via the previous one...

                        send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", new JsonObject()
                                                .put("type", "sessionToken")
                                                .put("sessionTokenHash", user.getString("sessionTokenHash")))
                                , authReply -> {
                                    context.assertTrue(authReply.succeeded());
                                    context.assertEquals(404, authReply.statusCode());
                                    testResult.countDown();
                                });
                    });
        });
        getNewUserAndAuthenticatedSession(new JsonArray().add("tester"), userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionAuthenticationMissingViaSessionToken(TestContext context) {
        Async testResult = context.async(3);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertTrue(registrationResult.succeeded());
            JsonObject user = registrationResult.result();

            JsonObject sessionTokenHashConfig = new JsonObject()
                    .put("hashFactory", "PBKDF2WithHmacSHA512")
                    .put("hashSalt", "testSalt")
                    .put("hashIterations", 10)
                    .put("hashLength", 256);

            // interrupt session
            send("userAuth", "1", "sessionInterrupted", new JsonObject()
                            .put("sessionTokenHash", user.getString("sessionTokenHash"))
                    , interruptionReply -> {
                        context.assertTrue(interruptionReply.succeeded());
                        context.assertEquals(200, interruptionReply.statusCode());
                        context.assertNotNull(interruptionReply.getBodyAsJsonObject().getLong("interruptedTimestamp"));
                        testResult.countDown();


                String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
                // skipping calculating a hash here

                send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                                .put("sessionTokenHash", token)
                                .put("sessionTokenHashConfig", sessionTokenHashConfig)
                        , reply -> {
                            context.assertTrue(reply.succeeded());
                            context.assertEquals(200, reply.statusCode());
                            testResult.countDown();

                            // authenticate this session via the previous one...

                            send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                            .put("sessionTokenHash", token)
                                            .put("authMethodConfig", new JsonObject()
                                                .put("type", "sessionToken")
                                                .put("sessionTokenHash", user.getString("sessionTokenHash")))
                                    , authReply -> {
                                        context.assertTrue(authReply.succeeded());
                                        context.assertEquals(200, authReply.statusCode());

                                        JsonObject result = authReply.getBodyAsJsonObject();
                                        context.assertNotNull(result.getString("userID"));
                                        context.assertEquals(user.getString("userID"), result.getString("userID"));
                                        context.assertNotNull(result.getJsonArray("userRoles"));
                                        context.assertEquals(1, result.getJsonArray("userRoles").size());
                                        context.assertTrue(result.getJsonArray("userRoles").contains("tester"));
                                        testResult.countDown();
                                    });
                        });
                    });
        });
        getNewUserAndAuthenticatedSession(new JsonArray().add("tester"), userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionAuthenticationMissingViaSessionTokenButSessionUnknown(TestContext context) {
        Async testResult = context.async(2);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertTrue(registrationResult.succeeded());
            JsonObject user = registrationResult.result();

            JsonObject sessionTokenHashConfig = new JsonObject()
                    .put("hashFactory", "PBKDF2WithHmacSHA512")
                    .put("hashSalt", "testSalt")
                    .put("hashIterations", 10)
                    .put("hashLength", 256);

            String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
            // skipping calculating a hash here

            send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                            .put("sessionTokenHash", token)
                            .put("sessionTokenHashConfig", sessionTokenHashConfig)
                    , reply -> {
                        context.assertTrue(reply.succeeded());
                        context.assertEquals(200, reply.statusCode());
                        testResult.countDown();

                        // authenticate this session via the previous one...

                        send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", new JsonObject()
                                                .put("type", "sessionToken")
                                                .put("sessionTokenHash", user.getString("sessionTokenHash")+"hy"))
                                , authReply -> {
                                    context.assertTrue(authReply.succeeded());
                                    context.assertEquals(404, authReply.statusCode());
                                    testResult.countDown();
                                });
                    });
        });
        getNewUserAndAuthenticatedSession(new JsonArray().add("tester"), userRegistrationFuture);

        testResult.awaitSuccess();
    }

    //@Test
    public void testProcessSessionAuthenticationMissingViaSessionTokenTwice(TestContext context) {
        Async testResult = context.async(4);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertTrue(registrationResult.succeeded());
            JsonObject user = registrationResult.result();

            JsonObject sessionTokenHashConfig = new JsonObject()
                    .put("hashFactory", "PBKDF2WithHmacSHA512")
                    .put("hashSalt", "testSalt")
                    .put("hashIterations", 10)
                    .put("hashLength", 256);

            String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
            // skipping calculating a hash here

            send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                            .put("sessionTokenHash", token)
                            .put("sessionTokenHashConfig", sessionTokenHashConfig)
                    , reply -> {
                        context.assertTrue(reply.succeeded());
                        context.assertEquals(200, reply.statusCode());
                        testResult.countDown();

                        // authenticate this session via the previous one...

                        send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("authMethodConfig", new JsonObject()
                                                .put("type", "sessionToken")
                                                .put("sessionTokenHash", user.getString("sessionTokenHash")))
                                , authReply -> {
                                    context.assertTrue(authReply.succeeded());
                                    context.assertEquals(200, authReply.statusCode());
                                    testResult.countDown();

                                    String token2 = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());

                                    send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                                                    .put("sessionTokenHash", token2)
                                                    .put("sessionTokenHashConfig", sessionTokenHashConfig)
                                            , reply2 -> {
                                                context.assertTrue(reply2.succeeded());
                                                context.assertEquals(200, reply2.statusCode());
                                                testResult.countDown();

                                                // authenticate this session via the previous one should fail because the previous session was closed

                                                send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                                                .put("sessionTokenHash", token2)
                                                                .put("authMethodConfig", new JsonObject()
                                                                        .put("type", "sessionToken")
                                                                        .put("sessionTokenHash", user.getString("sessionTokenHash")))
                                                        , authReply2 -> {
                                                            context.assertTrue(authReply2.succeeded());
                                                            context.assertEquals(401, authReply2.statusCode());
                                                            testResult.countDown();
                                                        });
                                            });
                                });
                    });
        });
        getNewUserAndAuthenticatedSession(new JsonArray().add("tester"), userRegistrationFuture);

        testResult.awaitSuccess();
    }

    @Test
    public void testProcessSessionAuthenticationMissingViaSessionTokenButSessionWasClosed(TestContext context) {
        Async testResult = context.async(3);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
            context.assertTrue(registrationResult.succeeded());
            JsonObject user = registrationResult.result();

            JsonObject sessionTokenHashConfig = new JsonObject()
                    .put("hashFactory", "PBKDF2WithHmacSHA512")
                    .put("hashSalt", "testSalt")
                    .put("hashIterations", 10)
                    .put("hashLength", 256);

            String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
            // skipping calculating a hash here

            send("userAuth", "1", "sessionEnded", new JsonObject()
                            .put("sessionTokenHash", user.getString("sessionTokenHash"))
                    , endReply -> {
                        context.assertTrue(endReply.succeeded());
                        context.assertEquals(200, endReply.statusCode());
                        context.assertNotNull(endReply.getBodyAsJsonObject().getLong("endTimestamp"));
                        testResult.countDown();


                        send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                                        .put("sessionTokenHash", token)
                                        .put("sessionTokenHashConfig", sessionTokenHashConfig)
                                , reply -> {
                                    context.assertTrue(reply.succeeded());
                                    context.assertEquals(200, reply.statusCode());
                                    testResult.countDown();

                                    // authenticate this session via the previous one...

                                    send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                                    .put("sessionTokenHash", token)
                                                    .put("authMethodConfig", new JsonObject()
                                                            .put("type", "sessionToken")
                                                            .put("sessionTokenHash", user.getString("sessionTokenHash")))
                                            , authReply -> {
                                                context.assertTrue(authReply.succeeded());
                                                context.assertEquals(404, authReply.statusCode());
                                                testResult.countDown();
                                            });
                                });
                    });
        });
        getNewUserAndAuthenticatedSession(new JsonArray().add("tester"), userRegistrationFuture);

        testResult.awaitSuccess();
    }


    @Test
    public void testProcessUserUnregistered(TestContext context) {
        Async testResult = context.async(4);

        Future<JsonObject> userRegistrationFuture = Future.future();
        userRegistrationFuture.setHandler(registrationResult -> {
                context.assertTrue(registrationResult.succeeded());
                JsonObject user = registrationResult.result();

                send("userAuth", "1", "userUnregistered", new JsonObject()
                                .put("userID", user.getString("userID"))
                        , reply -> {
                            context.assertTrue(reply.succeeded());
                            context.assertEquals(200, reply.statusCode());
                            testResult.countDown();

                            JsonObject sessionTokenHashConfig = new JsonObject()
                                    .put("hashFactory", "PBKDF2WithHmacSHA512")
                                    .put("hashSalt", "testSalt")
                                    .put("hashIterations", 10)
                                    .put("hashLength", 256);
                            String token = java.util.Base64.getEncoder().encodeToString(UUID.randomUUID().toString().getBytes());
                            // skipping calculating a hash here


                            send("userAuth", "1", "sessionRegistrationMissing", new JsonObject()
                                            .put("sessionTokenHash", token)
                                            .put("sessionTokenHashConfig", sessionTokenHashConfig)
                                    , reply2 -> {
                                        context.assertTrue(reply2.succeeded());
                                        context.assertEquals(200, reply2.statusCode());
                                        testResult.countDown();

                                        // try to get an authenticated session again via a previous session token
                                        send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                                        .put("sessionTokenHash", token)
                                                        .put("authMethodConfig", new JsonObject()
                                                                .put("type", "sessionToken")
                                                                .put("sessionTokenHash", user.getString("sessionTokenHash")))
                                                , authReply -> {
                                                    context.assertTrue(authReply.succeeded());
                                                    context.assertEquals(404, authReply.statusCode());
                                                    testResult.countDown();

                                                    // try to get authenticated session via email
                                                    send("userAuth", "1", "sessionAuthenticationMissing", new JsonObject()
                                                                    .put("sessionTokenHash", token)
                                                                    .put("authMethodConfig", sessionTokenHashConfig
                                                                            .put("type", "email")
                                                                            .put("emailAddress", user.getString("emailAddress"))
                                                                            .put("passwordHash", user.getString("passwordHash")))
                                                            , authReply2 -> {
                                                                context.assertTrue(authReply2.succeeded());
                                                                context.assertEquals(401, authReply2.statusCode());
                                                                testResult.countDown();
                                                            });
                                        });
                                    });
                        });
            });

        getNewUserAndAuthenticatedSession(new JsonArray().add("tester"), userRegistrationFuture);

        testResult.awaitSuccess();
    }

    private String getRandomMailAddress() {
        return UUID.randomUUID().toString() + "@test.de";
    }


}

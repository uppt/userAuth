package de.maltebehrendt.uppt.services;

import de.maltebehrendt.uppt.annotation.Payload;
import de.maltebehrendt.uppt.annotation.Processor;
import de.maltebehrendt.uppt.enums.DataType;
import de.maltebehrendt.uppt.events.Message;
import io.vertx.core.CompositeFuture;
import io.vertx.core.Future;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.UUID;
import java.util.regex.Pattern;

public class UserAuthService1 extends AbstractService {

    // TODO: replace those quick & dirty queries by optimized ones
    // TODO: notify bridges of session invalidations (session ended + user unregistered)
    // TODO: inform routers on login/logouts
    // TODO: inform routers with corresponding profiles of a user unregistration
    // TODO: if a db operation failed, then investigate why (further queries) and provide more meaningful error messages (see next)
    // TODO: adjust logic for checking multiple equal events, e.g. registration: find user via mail, if already available check 'progress' value, return message accordingly

    private static String AQL_checkIfEmailIsAlreadyUsedForAuth = "WITH authenticationMethods FOR a IN authenticationMethods FILTER a.emailAddress == @emailAddress LIMIT 1 RETURN {id: a._id, key: a._key}";
    private static String AQL_registerUserWithEmailAuth = "WITH users, authenticationMethods, has_authentication_method LET user = (INSERT @userDocument IN users RETURN {userDocumentKey: NEW._key, userDocumentID: NEW._id}) LET auth = (INSERT @authenticationDocument IN authenticationMethods RETURN {authDocumentID: NEW._id, authDocumentKey: NEW._id}) LET authEdge = (INSERT {_from: FIRST(user).userDocumentID, _to: FIRST(auth).authDocumentID, type: @authType} INTO has_authentication_method RETURN {hasAuthenticationMethodID: NEW._id, hasAuthenticationMethodKey: NEW._key}) RETURN {userDocumentKey: FIRST(user).userDocumentKey, authDocumentKey: FIRST(auth).authDocumentKey, hasAuthenticationMethodKey: FIRST(authEdge).hasAuthenticationMethodKey}";
    private static String AQL_registerUserWithEmailAuthOTFRoleCreation = "WITH users, authenticationMethods, has_authentication_method, roles, has_role LET uD = (INSERT @userDocument IN users RETURN {userDocumentKey: NEW._key, userDocumentID: NEW._id}) LET amD = (INSERT @authenticationDocument IN authenticationMethods RETURN {authDocumentID: NEW._id, authDocumentKey: NEW._id}) LET hamE = (INSERT {_from: FIRST(uD).userDocumentID, _to: FIRST(amD).authDocumentID, type: @authType} INTO has_authentication_method RETURN {hasAuthenticationMethodKey: NEW._key, hasAuthenticationMethodID: NEW._id}) LET rDs = (FOR role IN @roleNames UPSERT {name: role} INSERT {name: role, createdTimestamp: @timestamp, lastChange: @timestamp, origin: @origin} UPDATE {lastChange: @timestamp} IN roles RETURN {roleName: NEW.name, roleDocumentID: NEW._id, roleDocumentKey: NEW._key}) LET hrE = (FOR role IN rDs INSERT {_from: FIRST(uD).userDocumentID, _to: role.roleDocumentID, createdTimestamp: @timestamp, origin: @origin} INTO has_role RETURN {hasRoleID: NEW._id, hasRoleKey: NEW._key}) RETURN {userDocumentKey: FIRST(uD).userDocumentKey, authMethodDocumentKey: FIRST(amD).authDocumentKey, hasAuthenticationMethodKey: FIRST(hamE).hasAuthenticationMethodKey, roleDocuments: rDs, hasRoleEdges: hrE}";
    private static String AQL_getAuthMethodConfigOfEmail ="WITH authenticationMethods FOR a IN authenticationMethods FILTER a.emailAddress == @emailAddress LIMIT 1 RETURN {hashFactory: a.hashFactory, hashSalcat: a.hashSalt, hashIterations: a.hashIterations, hashLength: a.hashLength, status: a.status}";
    private static String AQL_insertAnonymousSessionDocument = "WITH sessions UPSERT {_key: @tokenHashAsKey} INSERT {_key: @tokenHashAsKey, sessionTokenHash: @tokenHash, startTimestamp: @timestamp, interruptedTimestamp: 0, endTimestamp: 0, tokenHashConfig: @tokenHashConfig, context: @sessionContext, routerAddress: @routerAddress, sessionReference: @sessionReference} UPDATE {endTimestamp: @timestamp} IN sessions RETURN {key: NEW._key, id: NEW._id, isOld: OLD ? true : false}";
    private static String AQL_insertAnonymousHasSessionEdge = "WITH has_session INSERT {_from: \"users/anonymous\", _to: @sessionDocumentID} INTO has_session RETURN {key: NEW._key, id: NEW._id}";

    private static String AQL_authenticateSessionViaEmail = "WITH authenticationMethods, has_authentication_method, has_role, is_authenticated_via, has_session LET aD = (FOR a IN authenticationMethods FILTER a.emailAddress == @emailAddress AND a.passwordHash == @passwordHash LIMIT 1 RETURN {id: a._id, key: a._key}) LET uDetails = (FOR r,e,p IN 1..2 INBOUND FIRST(aD).id has_authentication_method, OUTBOUND has_role RETURN {roleName: r.name, userID: p.vertices[1]._key, userDocumentID: p.vertices[1]._id}) LET iavE = (INSERT {_from: @sessionDocumentID, _to: FIRST(aD).id, timestamp: @timestamp, origin: @origin} INTO is_authenticated_via RETURN {id: NEW._id, key: NEW._key}) LET hsE = (FOR e IN has_session FILTER e._to == @sessionDocumentID LIMIT 1 RETURN {id: e._id, key: e._key, userDocumentID: e._from}) RETURN {authDocumentID: FIRST(aD).id, authDocumentKey: FIRST(aD).key, hasSessionKey: FIRST(hsE).key, isAuthViaKey: FIRST(iavE).key, isAuthViaID: FIRST(iavE).id, userDetails: uDetails, previousUserDocumentID: FIRST(hsE).userDocumentID}";
    private static String AQL_reassignAnonymousHasSessionAfterAuth = "WITH has_session UPDATE {_key: @hasSessionKey} WITH {_from: @userDocumentID} IN has_session RETURN {hasSessionID: NEW._id}";
    private static String AQL_authenticateSessionViaSessionToken = "WITH authenticationMethods, has_session, has_role, sessions, is_authenticated_via LET asD = (FOR s IN sessions FILTER s._key == @authSessionDocumentKey AND s.interruptedTimestamp > 0 AND s.endTimestamp == 0 LIMIT 1 RETURN {id: s._id, key: s._key}) LET uDetails = (FOR r,e,p IN 1..2 INBOUND FIRST(asD).id has_session, OUTBOUND has_role RETURN {roleName: r.name, userID: p.vertices[1]._key, userDocumentID: p.vertices[1]._id, hasSessionKey: p.edges[0]._key}) LET asDu = (UPDATE {_key: FIRST(asD).key} WITH {endTimestamp: @timestamp} IN sessions RETURN {id: NEW._id}) LET hsE = (UPDATE {_key: FIRST(uDetails).hasSessionKey} WITH {_to: @sessionDocumentID, origin: @origin, timestamp: @timestamp} IN has_session RETURN {id: NEW._id, oldOrigin: OLD.origin, oldTimestamp: OLD.timestamp}) LET hadsE = (INSERT {_from: FIRST(uDetails).userDocumentID, _to: FIRST(asD).id, origin: FIRST(hsE).oldOrigin, timestamp: FIRST(hsE).timestamp} INTO had_session RETURN {id: NEW._id}) LET iavE = (INSERT {_from: @sessionDocumentID, _to: FIRST(asD).id, timestamp: @timestamp} INTO is_authenticated_via RETURN {id: NEW._id}) RETURN {authSessionDocumentID: FIRST(asDu).id, userDetails: uDetails, hasSessionID: FIRST(hsE).id, hadSessionID: FIRST(hadsE).id, iAuthViaID: FIRST(iavE).id}";

    private static String AQL_markSessionAsInterrupted = "WITH sessions UPDATE {_key: @sessionDocumentKey, interruptedTimestamp: @interruptedTimestamp} IN sessions RETURN {oldInterruptedTimestamp: OLD.interruptedTimestamp}";
    private static String AQL_markSessionAsEnded = "WITH has_session, had_session, sessions LET sD = (UPDATE {_key: @sessionDocumentKey} WITH {endTimestamp: @endTimestamp} IN sessions RETURN {sessionDocumentID: NEW._id, oldEndTimestamp: OLD.endTimestamp}) LET hsE = (FOR e IN has_session FILTER e._to == FIRST(sD).sessionDocumentID LIMIT 1 RETURN {userDocumentID: e._from, key: e._key}) LET hadsE = (INSERT {_from: FIRST(hsE).userDocumentID, _to: FIRST(sD).sessionDocumentID, origin: @origin} INTO had_session RETURN {id: NEW._id}) REMOVE {_key: FIRST(hsE).key} IN has_session RETURN {hasSessionID: OLD._id, hadSessionID: FIRST(hadsE).id, sessionDocumentID: FIRST(sD).sessionDocumentID, oldEndTimestamp: FIRST(sD).oldEndTimestamp}";
    private static String AQL_markAllSessionsAsEnded = "WITH has_session, sessions, had_session LET hasSessions = (FOR v,e IN 1..1 OUTBOUND @userDocumentID has_session RETURN {sessionDocumentID: v._id, sessionDocumentKey: v._key, hasSessionDocumentID: e._id, hasSessionDocumentKey: e._key}) FOR hs IN hasSessions LET hadSessionEdge = (INSERT {_from: @userDocumentID, _to: hs.sessionDocumentID} INTO had_session RETURN {id: NEW._id}) LET sessionDocument = (UPDATE {_key: hs.sessionDocumentKey} WITH {endTimestamp: @endTimestamp} IN sessions RETURN {id: NEW._id}) LET hasSessionEdge = (REMOVE {_key: hs.hasSessionDocumentKey} IN has_session RETURN {id: OLD._id}) RETURN {hasSessionEdgeID: FIRST(hasSessionEdge).id, sessionDocumentID: FIRST(sessionDocument).id, hadSessionEdgeID: FIRST(hadSessionEdge).id}";

    private static String AQL_getUserAuthenticationMethods = "WITH users, has_authentication_method LET uD = (UPDATE {_key: @userDocumentKey} WITH {invalidatedTimestamp: @timestamp, origin: @origin} IN users RETURN {userDocumentID: NEW._id}) FOR aumE IN has_authentication_method FILTER aumE._from == FIRST(uD).userDocumentID RETURN {authenticationMethodDocumentID: aumE._to}";

    private Pattern emailPattern = null;

    @Processor(
            domain = "userAuth",
            version = "1",
            type = "sessionAuthenticationMissing",
            description = "Either registers a new, anonymous session or - if a session token hash is provided - performs a session handover",
            requires = {
                    @Payload(key = "sessionTokenHash", type = DataType.STRING, description = "Session token hash of the current session"),
                    @Payload(key = "authMethodConfig", type = DataType.JSONObject, description = "Must contain string 'type' denoting authentication method (currently only 'email' and 'sessionToken' are supported) and all required parameters, e.g. password hash, email address, ...")
            },
            provides = {
                    @Payload(key = "userID", type = DataType.STRING, description = "ID of the user that started the session"),
                    @Payload(key = "userRoles", type = DataType.STRING, description = "Roles of the user that started the session")
            }
    )
    public void processSessionAuthenticationMissing(Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        String sessionTokenHash = body.getString("sessionTokenHash");
        JsonObject authenticationMethod = body.getJsonObject("authMethodConfig");
        if(sessionTokenHash == null || sessionTokenHash.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHash");
            return;
        }
        if(authenticationMethod == null || !authenticationMethod.containsKey("type")) {
            message.fail(400, "Must provide authMethodConfig");
            return;
        }

        // ensuring the token is a valid key for ArangoDB...
        String sessionTokenHashAsKey = sessionTokenHash.replace("/", "_");
        String sessionTokenHashAsID = "sessions/" + sessionTokenHashAsKey;
        Long timestamp = System.currentTimeMillis();

        String authenticationType = authenticationMethod.getString("type");
        if("email".equals(authenticationType)) {
            String emailAddress = authenticationMethod.getString("emailAddress");
            String passwordHash = authenticationMethod.getString("passwordHash");

            if (emailAddress == null) {
                message.fail(400, "authenticationMethod must contain entry 'emailAddress' for identifying the settings for the user");
                return;
            }
            if(passwordHash == null || passwordHash.isEmpty()) {
                message.fail(400, "authenticationMethod must contain entry 'passwordHash' with the user's password hash");
                return;
            }


            Future<JsonArray> authFuture = Future.future();
            authFuture.setHandler(authResult -> {
               if(authResult.failed()) {
                   String errorMessage = authResult.cause().getMessage();
                   if(errorMessage.contains("400") && errorMessage.contains("1233")) {
                       logger.error(message.correlationID(), emailAddress, 401, "Failed to check user authentication - email or password incorrect: " + errorMessage);
                       message.fail(401,"Email or password incorrect");
                   }
                   else {
                       logger.fatal(message.correlationID(), emailAddress, 500, "Failed to check user authentication", authResult.cause());
                       message.fail("Failed to check authentication for an unknown reason");
                   }
                   return;
               }

               JsonObject resultObject = authResult.result().getJsonObject(0);
               if(resultObject == null || resultObject.isEmpty() || !resultObject.containsKey("userDetails") || !resultObject.containsKey("previousUserDocumentID")) {
                   logger.error(message.correlationID(), emailAddress, 500, "Failed to check user authentication - empty result set!");
                   message.fail("Failed to check authentication for an unknown reason");
                   return;
               }

               if(!resultObject.containsKey("hasSessionKey") || resultObject.getString("hasSessionKey") == null) {
                   logger.error(message.correlationID(), emailAddress, 400, "Invalid/unknown session!");
                   message.fail(404,"Provided session token could not be attributed to a valid session!");
                   return;
               }

               String previousUserDocumentID = resultObject.getString("previousUserDocumentID");
               String anonymousUserDocumentID = "users/anonymous";
               String userDocumentID = null;
               String userID = null;
               JsonArray userDetails = resultObject.getJsonArray("userDetails");

               JsonArray userRoles = new JsonArray();
               for(int i=0;i<userDetails.size();i++) {
                    JsonObject userDetail = userDetails.getJsonObject(i);
                    if(userDetail == null || userDetail.isEmpty()) {
                        continue;
                    }
                    String uDID = userDetail.getString("userDocumentID");
                    String uID = userDetail.getString("userID");

                    userDocumentID = uDID != null? uDID : userDocumentID;
                    userID = uID != null? uID : userID;
                    String roleName = userDetail.getString("roleName");

                    if(userDocumentID != null && !anonymousUserDocumentID.equals(userDocumentID) && previousUserDocumentID.equals(userDocumentID)) {
                        logger.security(message.correlationID(), emailAddress, 403, "Owner of session (which is to be authenticated) is nether the anonymous user nor the user who just authenticated itself. Please investigate: " + resultObject.encodePrettily());
                        message.fail(403, "Security violation");
                        return;
                    }
                    if(roleName != null) {
                        userRoles.add(roleName);
                    }
               }

               JsonObject replyObject = new JsonObject()
                       .put("userID", userID)
                       .put("userRoles", userRoles);

               Future<JsonArray> reassignFuture = Future.future();
               reassignFuture.setHandler(reassignResult -> {
                   if (reassignResult.failed()) {
                       logger.error(message.correlationID(), emailAddress, 500, "Failed to reassign session after authentication", reassignResult.cause());
                       message.fail("Failed to reassign session for an unknown reason");
                       reassignResult.cause().printStackTrace();
                       return;
                   }
                   message.reply(replyObject);
               });
               Map<String, Object> paramMap = new HashMap<>();
               paramMap.put("userDocumentID", userDocumentID);
               paramMap.put("hasSessionKey", resultObject.getString("hasSessionKey"));
               database.query(AQL_reassignAnonymousHasSessionAfterAuth, paramMap, reassignFuture);
            });
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("emailAddress", emailAddress);
            paramMap.put("passwordHash", passwordHash);
            paramMap.put("sessionDocumentID", sessionTokenHashAsID);
            paramMap.put("timestamp", timestamp);
            paramMap.put("origin", message.origin());
            database.query(AQL_authenticateSessionViaEmail, paramMap, authFuture);
        }
        else if("sessionToken".equals(authenticationType)) {
            String authSessionTokenHash = authenticationMethod.getString("sessionTokenHash");
            if(authSessionTokenHash == null || authSessionTokenHash.isEmpty()) {
                message.fail(400, "authenticationMethod must contain entry 'sessionTokenHash' for a non-closed but interrupted session");
                return;
            }

            String authSessionTokenHashAsKey = authSessionTokenHash.replace("/", "_");

            Future<JsonArray> authFuture = Future.future();
            authFuture.setHandler(authResult -> {
                if (authResult.failed()) {
                    String errorMessage = authResult.cause().getMessage();
                    if (errorMessage != null && errorMessage.contains("400") && errorMessage.contains("1233")) {
                        message.fail(401, "Previous session invalid, e.g. was closed or does not exist");
                    } else if (errorMessage != null && errorMessage.contains("400") && errorMessage.contains("1226")) {
                        message.fail(404, "Previous session not found");
                    } else {
                        logger.security(message.correlationID(), message.origin(), 500, "Failed to check user authentication via previous session for an unknown reason", authResult.cause());
                        message.fail("Failed to check authentication for an unknown reason");
                    }
                    return;
                }

                JsonObject resultObject = authResult.result().getJsonObject(0);
                if(resultObject == null || resultObject.isEmpty() || !resultObject.containsKey("userDetails")) {
                    logger.error(message.correlationID(), message.origin(), 500, "Failed to check user authentication - empty result set!");
                    message.fail("Failed to check authentication for an unknown reason");
                    return;
                }

                String userID = null;
                JsonArray userDetails = resultObject.getJsonArray("userDetails");

                JsonArray userRoles = new JsonArray();
                for(int i=0;i<userDetails.size();i++) {
                    JsonObject userDetail = userDetails.getJsonObject(i);
                    if(userDetail == null || userDetail.isEmpty()) {
                        continue;
                    }
                    userID = userDetail.getString("userID");
                    String roleName = userDetail.getString("roleName");

                    if(roleName != null) {
                        userRoles.add(roleName);
                    }
                }


                JsonObject replyObject = new JsonObject()
                        .put("userID", userID)
                        .put("userRoles", userRoles);

                message.reply(replyObject);
            });
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("authSessionDocumentKey", authSessionTokenHashAsKey);
            paramMap.put("sessionDocumentID", sessionTokenHashAsID);
            paramMap.put("timestamp", timestamp);
            paramMap.put("origin", message.origin());
            database.query(AQL_authenticateSessionViaSessionToken, paramMap, authFuture);
        }
        else {
            logger.warn(message.correlationID(), message.origin(), 501, "Unknown authentication type : " + authenticationType);
            message.fail(501, "AuthType is not known/supported.");
            return;
        }
    }

    @Processor(
            domain = "userAuth",
            version = "1",
            type = "sessionInterrupted",
            description = "Marks a registered session as interrupted (e.g. because the connection was lost)",
            requires = {
                    @Payload(key = "sessionTokenHash", type = DataType.STRING, description = "Hash of an existing session token"),
            },
            provides = {
                    @Payload(key = "interruptedTimestamp", type = DataType.LONG, description = "Timestamp of session interruption"),
            }
    )
    public void processSessionInterrupted(Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        String sessionTokenHash = body.getString("sessionTokenHash");
        if(sessionTokenHash == null || sessionTokenHash.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHash");
            return;
        }

        // ensuring the token is a valid key for ArangoDB...
        String sessionTokenHashAsKey = sessionTokenHash.replace("/", "_");
        Long interruptedTimestamp = System.currentTimeMillis();

        Future<JsonArray> interruptedFuture = Future.future();
        interruptedFuture.setHandler(interruptedResult -> {
            if (interruptedResult.failed()) {
                logger.error(message.correlationID(), message.origin(), 404, "Failed to mark session as interrupted, session does not seem to exist", interruptedResult.cause());
                message.fail(404, "Session does not seem to exist");
                return;
            }

            JsonObject queryResult = interruptedResult.result().getJsonObject(0);

            if(queryResult == null || !queryResult.containsKey("oldInterruptedTimestamp")) {
                logger.error(message.correlationID(), message.origin(), 500, "Failed to mark session as interrupted - empty result set");
                message.fail("Failed to mark session as interrupted for an unknown reason");
                return;
            }

            if(queryResult.getLong("oldInterruptedTimestamp") != 0L) {
                message.fail(404, "Session was already marked as interrupted!");
                return;
            }

            message.reply(new JsonObject().put("interruptedTimestamp", interruptedTimestamp));
        });

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("sessionDocumentKey", sessionTokenHashAsKey);
        paramMap.put("interruptedTimestamp", interruptedTimestamp);
        database.query(AQL_markSessionAsInterrupted, paramMap, interruptedFuture);
    }

    @Processor(
            domain = "userAuth",
            version = "1",
            type = "sessionEnded",
            description = "Marks a registered session as ended/closed/invalid (actively ended by request - not an interruption)",
            requires = {
                    @Payload(key = "sessionTokenHash", type = DataType.STRING, description = "Hash of an existing session token"),
            },
            provides = {
                    @Payload(key = "endTimestamp", type = DataType.LONG, description = "Timestamp of session end"),
            }
    )
    public void processSessionEnded(Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        String sessionTokenHash = body.getString("sessionTokenHash");
        if(sessionTokenHash == null || sessionTokenHash.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHash");
            return;
        }

        // ensuring the token is a valid key for ArangoDB...
        String sessionTokenHashAsKey = sessionTokenHash.replace("/", "_");
        Long endTimestamp = System.currentTimeMillis();

        Future<JsonArray> endFuture = Future.future();
        endFuture.setHandler(endResult -> {
            if (endResult.failed()) {
                logger.error(message.correlationID(), message.origin(), 404, "Failed to end session, session does seem to exist or has already ended", endResult.cause());
                message.fail(404,"Failed to end session, session doesn't seem to exist or has already ended!");
                return;
            }

            JsonObject queryResult = endResult.result().getJsonObject(0);

            if(queryResult == null || queryResult.isEmpty() || !queryResult.containsKey("oldEndTimestamp") || !queryResult.containsKey("hadSessionID")) {
                logger.error(message.correlationID(), message.origin(), 500, "Failed to end session for an unknown reason - result set empty!");
                message.fail("Failed to end session for an unknown reason");
                return;
            }
            message.reply(new JsonObject().put("endTimestamp", endTimestamp));
        });

        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("sessionDocumentKey", sessionTokenHashAsKey);
        paramMap.put("endTimestamp", endTimestamp);
        paramMap.put("origin", message.origin());
        database.query(AQL_markSessionAsEnded, paramMap, endFuture);
    }


    @Processor(
            domain = "userAuth",
            version = "1",
            type = "sessionRegistrationMissing",
            description = "Registers a new anonymous session or an authenticated session authorized via a previous authenticated session token (the previous token becomes invalid)",
            requires = {
                    @Payload(key = "sessionTokenHash", type = DataType.STRING, description = "Either hash of an existing session token or the session token hash to be set"),
                    @Payload(key = "sessionTokenHashConfig", type = DataType.JSONObject, description = "Hashing configuration, i.e. hashFactory, hashSalt, hashIterations, hashLength")
            },
            optional = {
                    @Payload(key = "sessionContext", type = DataType.JSONObject, description = "Details on the session context, e.g. router, device infos, ..."),
                    @Payload(key = "sessionConfig", type = DataType.JSONObject, description = "Configuration of the session, includes in particular the router's address and set traits")
                    //TODO router address
            },
            provides = {
                    @Payload(key = "userID", type = DataType.STRING, description = "ID of the user that started the session"),
                    @Payload(key = "userRoles", type = DataType.STRING, description = "Roles of the user that started the session"),
                    @Payload(key = "sessionTokenHash", type = DataType.STRING, description = "Hash of the session token (changed if a session handover was performed)")
            }
    )
    public void processSessionRegistrationMissing(Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        String sessionTokenHash = body.getString("sessionTokenHash");
        JsonObject sessionTokenHashConfig = body.getJsonObject("sessionTokenHashConfig");
        if(sessionTokenHashConfig == null || sessionTokenHashConfig.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHashConfig");
            return;
        }
        if(sessionTokenHash == null || sessionTokenHash.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHash");
            return;
        }

        String hashFactory = sessionTokenHashConfig.getString("hashFactory");
        String hashSalt = sessionTokenHashConfig.getString("hashSalt");
        Integer hashIterations = sessionTokenHashConfig.getInteger("hashIterations");
        Integer hashLength = sessionTokenHashConfig.getInteger("hashLength");
        if(hashFactory == null || hashFactory.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHashConfig.hashFactory");
            return;
        }
        if(hashSalt == null || hashSalt.isEmpty()) {
            message.fail(400, "Must provide non-empty sessionTokenHashConfig.hashSalt");
            return;
        }
        if(hashIterations == null || hashIterations < config().getJsonObject("hashPolicy").getInteger("minimumIterations")) {
            message.fail(400, "sessionTokenHashConfig.hashIterations is smaller than required minimum of " + config().getJsonObject("hashPolicy").getInteger("minimumIterations"));
            return;
        }
        if(hashLength == null || hashLength < config().getJsonObject("hashPolicy").getInteger("minimumLength")) {
            message.fail(400, "sessionTokenHashConfig.hashLength is smaller than required minimum of " + config().getJsonObject("hashPolicy").getInteger("minimumLength"));
            return;
        }

        JsonObject sessionContext = body.containsKey("sessionContext") ? body.getJsonObject("sessionContext") : new JsonObject();
        JsonObject routerAddress = sessionContext.containsKey("routerAddress")? sessionContext.getJsonObject("routerAddress") : new JsonObject();
        //TODO
        JsonObject sessionConfig =  body.containsKey("sessionContext") ? body.getJsonObject("sessionConfig") : new JsonObject();
        String sessionReference = UUID.randomUUID().toString();
        Long timestamp = System.currentTimeMillis();

        // ensuring the token is a valid key for ArangoDB...
        String sessionTokenHashAsKey = sessionTokenHash.replace("/", "_");


        Future<JsonArray> insertFuture = Future.future();
        insertFuture.setHandler(insertResult -> {
            if(insertResult.failed()) {
                logger.error(message.correlationID(), message.origin(), 500, "Failed to insert anonymous session document", insertResult.cause());
                message.fail("Failed to create anonymous session for an unknown reason");
                return;
            };

            JsonObject result = insertResult.result().getJsonObject(0);
            String sessionDocumentID = result.getString("id");

            if(result.getBoolean("isOld")) {
                // session handover
                message.fail(500, "HANDOVER NOT IMPLEMENTED YET");
                return;
            }
            else {
                // is new session, insert has_session edge
                Future<JsonArray> edgeFuture = Future.future();
                edgeFuture.setHandler(edgeResult -> {
                   if(edgeResult.failed()) {
                       logger.error(message.correlationID(), message.origin(), 500, "Failed to insert anonymous has session edge", edgeResult.cause());
                       message.fail("Failed to create anonymous session for an unknown reason");
                       return;
                   }

                    message.reply(new JsonObject()
                            .put("userID", "anonymous")
                            .put("userRoles", new JsonArray())
                            .put("sessionTokenHash", sessionTokenHash));
                });
                Map<String, Object> paramMap = new HashMap<>();
                paramMap.put("sessionDocumentID", sessionDocumentID);
                database.query(AQL_insertAnonymousHasSessionEdge, paramMap, edgeFuture);
            }
        });
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("tokenHashAsKey", sessionTokenHashAsKey);
        paramMap.put("tokenHash", sessionTokenHash);
        paramMap.put("timestamp", timestamp);
        paramMap.put("tokenHashConfig", database.jsonObjectToParameterMap(sessionTokenHashConfig));
        paramMap.put("sessionContext", database.jsonObjectToParameterMap(sessionContext));
        paramMap.put("routerAddress", routerAddress);
        paramMap.put("sessionReference", sessionReference);
        database.query(AQL_insertAnonymousSessionDocument, paramMap, insertFuture);
    }


    @Processor(
            domain = "userAuth",
            version = "1",
            type = "authMethodConfigMissing",
            description = "Returns the auth method configuration parameters required for processing login requests",
            requires = {
                    @Payload(key = "authMethodIdentifier", type = DataType.JSONObject, description = "Must contain entry 'type' (e.g. 'email') and, if necessary, parameters for identifying the user, e.g. 'emailAddress'")
            },
            provides = {
                    @Payload(key = "authMethodConfig", type = DataType.JSONObject, description = "Auth method parameters"),
            })
    public void processAuthMethodConfigMissing(Message message) {
        JsonObject body = message.getBodyAsJsonObject();

        JsonObject authMethodIdentifier = body.getJsonObject("authMethodIdentifier");
        if(authMethodIdentifier == null || !authMethodIdentifier.containsKey("type")) {
            message.fail(400, "Must provide authMethodIdentifier and include type specification");
            return;
        }

        String authenticationType = authMethodIdentifier.getString("type");
        if("email".equals(authenticationType)) {

            String emailAddress = authMethodIdentifier.getString("emailAddress");
            if(emailAddress == null) {
                message.fail(400, "Must provide 'emailAddress' for identifying the settings for the user");
                return;
            }

            Future<JsonArray> getFuture = Future.future();
            getFuture.setHandler(getResult -> {
                if (getResult.failed()) {
                    logger.error(message.correlationID(), message.origin(), 500, "Failed to get auth method config.", getResult.cause());
                    message.fail("Failed to get auth method config for an unknown reason");
                    return;
                }

                JsonObject result = getResult.result().isEmpty()? null : getResult.result().getJsonObject(0);

                if(result == null || result.isEmpty() || !result.containsKey("hashFactory") || result.getString("hashFactory") == null) {
                    message.fail(404, "No user with this email address as authentication method found");
                    return;
                }
                if(!"verified".equals(result.getString("status")) && config().getJsonObject("userRegistration").getJsonObject("email").getBoolean("isEMailVerificationRequired")) {
                    message.fail(412, "Email address is not verified: " + result.getString("status"));
                    return;
                }

                message.reply(new JsonObject().put("authMethodConfig", result));
            });
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("emailAddress", emailAddress);
            database.query(AQL_getAuthMethodConfigOfEmail, paramMap, getFuture);
        }
        else {
            logger.warn(message.correlationID(), message.origin(), 501, "Unknown authentication type : " + authenticationType);
            message.fail(501, "AuthType is not known/supported.");
            return;
        }
    }

    @Processor(
            domain = "userAuth",
            version = "1",
            type = "userUnregistered",
            description = "Invalidates a user registration (removes auth methods) and deletes personal data (notification/contact info and profiles)",
            enablingConditionKey = "isUserRegistrationAllowed",
            requires = {
                    @Payload(key = "userID", type = DataType.STRING, description = "ID of the user that has unregistered")
            })
    public void processUserUnregistered(Message message) {
        JsonObject userRegistration = config().getJsonObject("userRegistration");
        if(!userRegistration.getJsonArray("whitelistByServiceOrigin").contains("*")
                && !userRegistration.getJsonArray("whitelistByServiceOrigin").contains(message.origin())) {
            logger.security(message.correlationID(), message.origin(), 403, "Unauthorized attempt to unregister a user. Check 'userRegistration.whitelistByServiceOrigin' in user service config");
            message.fail(403, "Origin " + message.origin() + " is not allowed to unregister users." );
            return;
        }

        JsonObject body = message.getBodyAsJsonObject();
        String userID = body.getString("userID");
        if(userID == null || userID.isEmpty()) {
            message.fail(400, "Must provide non-empty userID");
            return;
        }

        Long timestamp = System.currentTimeMillis();
        String userDocumentID = "users/" + userID;

        Future<JsonArray> invalidateSessionsFuture = Future.future();
        invalidateSessionsFuture.setHandler(invalidateSessionsResult -> {
            if(invalidateSessionsResult.failed()) {
                logger.warn(message.correlationID(), 404, "Failed to unregister user "  + userID + " - user not found", invalidateSessionsResult.cause());
                message.reply(404, "User not found");
                return;
            }

            JsonObject resultObject = invalidateSessionsResult.result().getJsonObject(0);
            if(resultObject == null) {
                logger.error(message.correlationID(), 500, "Failed to unregister user " + userID + " - empty result set!");
                message.fail("Failed to unregister due to an unknown error!");
                return;
            }

            Future<JsonArray> amFuture = Future.future();
            amFuture.setHandler(amResult -> {
                if(amResult.failed()) {
                    logger.error(message.correlationID(), message.origin(), 500, "Failed to identify all authentication method entries when unregistering user " + userID, amResult.cause());
                    message.fail(500, "Failed due to an unknown reason!");
                }

                JsonArray resultArray = amResult.result();
                LinkedList<Future> futures = new LinkedList();

                for(int i=0;i<resultArray.size();i++) {
                    Future<Void> future = Future.future();
                    futures.add(future);
                    String authenticationMethodDocumentID = resultArray.getJsonObject(i).getString("authenticationMethodDocumentID");
                    String authenticationMethodDocumentKey = authenticationMethodDocumentID.substring(authenticationMethodDocumentID.indexOf("/")+1);
                    database.deleteVertex("userAuthGraph", "authenticationMethods", authenticationMethodDocumentKey, future);
                }

                CompositeFuture.join(futures).setHandler(deleteResult -> {
                   if(deleteResult.failed()) {
                       logger.error(message.correlationID(), message.origin(), 500, "Failed to delete all authentication method entries when unregistering user " + userID, deleteResult.cause());
                       message.fail(500, "Failed due to an unknown reason!");
                       return;
                   }
                   logger.info(message.correlationID(), message.correlationID(), 200, "Unregistered user " + userID);

                   // TODO: inform all services to delete locally stored user data

                   message.reply(200);
                });
            });
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("userDocumentKey", userID);
            paramMap.put("timestamp", timestamp);
            paramMap.put("origin", message.origin());
            database.query(AQL_getUserAuthenticationMethods, paramMap,amFuture);

        });
        Map<String, Object> paramMap = new HashMap<>();
        paramMap.put("userDocumentID", userDocumentID);
        paramMap.put("endTimestamp", timestamp);
        database.query(AQL_markAllSessionsAsEnded, paramMap,invalidateSessionsFuture);
    }



    @Processor(
        domain = "userAuth",
        version = "1",
        type = "userRegistered",
        description = "Registers a new user in the database and adds corresponding nodes/edges to the database",
        enablingConditionKey = "isUserRegistrationAllowed",
        requires = {
                @Payload(key = "authMethodConfig", type = DataType.JSONObject, description = "Must contain string 'type' denoting authentication method (currently only 'email' is supported and all required parameters, e.g. password hash, email address, ..."),
                @Payload(key = "solutionAddresses", type = DataType.JSONArray, description = "Array with JsonObjects with the addresses of solutions which have profiles of the users. Must contain at least one entry (the solution via which the user is registering)")
        },
        optional = {
                @Payload(key = "userRoles", type = DataType.JSONArray, description = "Roles of the user that is to be registered. Origin must be whitelisted"),
                @Payload(key = "commonProfile", type = DataType.JSONObject, description = "Key/values to be stored in the user's common profile, i.e. accessible for all services. E.g. name, gender, ...")
        },
        provides = {
                @Payload(key = "userID", type = DataType.STRING, description = "ID of the user that has been registered"),
                @Payload(key = "userRoles", type = DataType.STRING, description = "Roles of the user that has been registered")
        })
    public void processUserRegistered(Message message) {
        JsonObject userRegistration = config().getJsonObject("userRegistration");
        if(!userRegistration.getJsonArray("whitelistByServiceOrigin").contains("*")
            && !userRegistration.getJsonArray("whitelistByServiceOrigin").contains(message.origin())) {
            logger.security(message.correlationID(), message.origin(), 403, "Unauthorized attempt to register a user. Check 'userRegistration.whitelistByServiceOrigin' in user service config");
            message.fail(403, "Origin " + message.origin() + " is not allowed to register users." );
            return;
        }

        JsonObject body = message.getBodyAsJsonObject();
        JsonObject authenticationMethod = body.getJsonObject("authMethodConfig");
        if(authenticationMethod == null || !authenticationMethod.containsKey("type")) {
            message.fail(400, "Must provide authMethodConfig");
            return;
        }
        JsonArray solutionAddresses = body.getJsonArray("solutionAddresses");
        if(solutionAddresses == null || solutionAddresses.isEmpty()) {
            message.fail(400, "Must provide non-empty 'solutionAddresses'");
            return;
        }
        for(int i=0;i<solutionAddresses.size();i++) {
            JsonObject address = solutionAddresses.getJsonObject(i);
            if(address == null || address.isEmpty() || !address.containsKey("domain") || !address.containsKey("type") || !address.containsKey("version")) {
                message.fail(400, "All entries of 'solutionAddresses' must be JsonObject containing 'domain', 'version', 'type'. Failed for entry: " + i);
                return;
            }
            String domain = address.getString("domain");
            String version = address.getString("version");
            String type = address.getString("type");
            if(domain == null || domain.isEmpty()) {
                message.fail(400, "Entry 'solutionAddresses[" + i + "].domain' must be non-empty string!");
                return;
            }
            if(version == null || version.isEmpty()) {
                message.fail(400, "Entry 'solutionAddresses[" + i + "].version' must be non-empty string!");
                return;
            }
            if(type == null || type.isEmpty()) {
                message.fail(400, "Entry 'solutionAddresses[" + i + "].type' must be non-empty string!");
                return;
            }
        }

        JsonArray userRoles = body.containsKey("userRoles")? body.getJsonArray("userRoles"): new JsonArray();
        if(!userRoles.isEmpty()
                && !config().getJsonObject("roleManagement").getJsonArray("whitelistByServiceOrigin").contains("*")
                && !config().getJsonObject("roleManagement").getJsonArray("whitelistByServiceOrigin").contains(message.origin())) {
            logger.security( message.correlationID(), message.origin(), 401, "Unauthorized attempt to register a user with roles. Check 'roleManagement.whitelistByServiceOrigin' in user service config");
            message.fail(401, "Origin " + message.origin() + " is not allowed to register users with roles." );
            return;
        }

        // prepare user document
        Long timestamp = System.currentTimeMillis();
        JsonObject userDocument = new JsonObject()
                .put("commonProfile", body.containsKey("commonProfile")? body.getJsonObject("commonProfile"): new JsonObject())
                .put("solutionAddresses", solutionAddresses)
                .put("createdTimestamp", timestamp)
                .put("origin", message.origin());

        String authenticationType = authenticationMethod.getString("type");

        if("email".equals(authenticationType)) {
            String emailAddress = authenticationMethod.getString("emailAddress");
            String passwordHash = authenticationMethod.getString("passwordHash");
            String hashFactory = authenticationMethod.getString("hashFactory");
            String hashSalt = authenticationMethod.getString("hashSalt");
            Integer hashIterations = authenticationMethod.getInteger("hashIterations");
            Integer hashLength = authenticationMethod.getInteger("hashLength");

            if(emailAddress == null || emailAddress.isEmpty() || !emailPattern.matcher(emailAddress).matches()) {
                message.fail(400, "authenticationMethod must contain entry 'emailAddress' with the user's email address and confirm to the configured valid mail pattern. Provided mail is empty or invalid: " + emailAddress);
                return;
            }
            if(passwordHash == null || passwordHash.isEmpty()) {
                message.fail(400, "authenticationMethod must contain entry 'passwordHash' with the user's password hash");
                return;
            }
            if(hashFactory == null || hashFactory.isEmpty()) {
                message.fail(400, "authenticationMethod must contain entry 'hashFactory' with the used password key factory's name");
                return;
            }
            if(hashSalt == null || hashSalt.isEmpty()) {
                message.fail(400, "authenticationMethod must contain entry 'hashSalt' with the used password salt");
                return;
            }
            if(hashSalt == null || hashSalt.isEmpty()) {
                message.fail(400, "authenticationMethod must contain entry 'hashSalt' with the used password salt");
                return;
            }
            if(hashIterations == null || hashIterations < config().getJsonObject("hashPolicy").getInteger("minimumIterations")) {
                message.fail(400, "authenticationMethod must contain entry 'hashIterations' defining at least " + config().getJsonObject("hashPolicy").getInteger("minimumIterations")  + " iterations. More is better");
                return;
            }
            if(hashLength == null || hashLength < config().getJsonObject("hashPolicy").getInteger("minimumLength")) {
                message.fail(400, "authenticationMethod must contain entry 'hashLength' defining a length of at least " + config().getJsonObject("hashPolicy").getInteger("minimumLength")  + ". More is better");
                return;
            }

            authenticationMethod.put("status", "unverified");
            authenticationMethod.put("createdTimestamp", timestamp);

            // check if user's email address is already known
            Future<JsonArray> checkFuture = Future.future();
            checkFuture.setHandler(checkResult -> {
                if(checkResult.failed()) {
                    logger.error(message.correlationID(), message.origin(), 500, "Failed to register user. Checking if its email is already used failed.", checkResult.cause());
                    message.fail("Failed to register user for an unknown reason");
                    return;
                }

                if(!checkResult.result().isEmpty()) {
                    logger.warn(message.correlationID(), emailAddress, 409, "Rejected user registration attempt as email is already used by another account");
                    message.fail(409, "Email address already in use for notifications by a user account");
                    return;
                }

                // register user
                Future<JsonArray> registerFuture = Future.future();
                registerFuture.setHandler(registerResult -> {
                    if(registerResult.failed()) {
                        logger.error(message.correlationID(), message.origin(), 500, "Failed to register user", registerResult.cause());
                        message.fail("Failed to register user for an unknown reason");
                        return;
                    }

                    JsonObject result = registerResult.result().getJsonObject(0);
                    if(result == null || result.isEmpty()) {
                        logger.error(message.correlationID(), message.origin(), 500, "Failed to register new user with email " + emailAddress + ". Empty result!");
                        message.fail(500, "Registration incomplete due to an unknown internal error!");
                        return;
                    }

                    String userID = result.getString("userDocumentKey");

                    // check that all roles have been linked
                    if(!userRoles.isEmpty() && (result.containsKey("roles"))) {
                        JsonArray roles = result.getJsonArray("roles");
                        JsonArray hasRole = result.getJsonArray("hasRole");

                        if(roles == null || hasRole == null || roles.size() != userRoles.size() || hasRole.size() != userRoles.size()) {
                            logger.error(message.correlationID(), message.origin(), 500, "Failed to register new user with email " + emailAddress + ". Not all required roles were linked. Please investigate: " + result.encodePrettily());
                            message.fail(500, "Registration incomplete due to an unknown internal error!");
                            return;
                        }
                    }

                    JsonObject replyMessage = new JsonObject()
                            .put("userID", userID)
                            .put("userRoles", userRoles);

                    if(userRegistration.getJsonObject("email").getBoolean("isEMailVerificationRequired")) {
                        // TODO: send mail for verification and store key in DB
                        logger.fatal(message.correlationID(), "Mail verification not impl yet");
                        replyMessage.put("status", "deactivated")
                            .put("message", "Missing email verification");
                    }
                    else {
                        replyMessage.put("status", "active");
                    }

                    message.reply(200, replyMessage);
                });

                Map<String, Object> paramMap = new HashMap<>();
                paramMap.put("userDocument", database.jsonObjectToParameterMap(userDocument));
                paramMap.put("authenticationDocument", database.jsonObjectToParameterMap(authenticationMethod));
                paramMap.put("authType", "email");

                if(userRoles.isEmpty()) {
                    database.query(AQL_registerUserWithEmailAuth, paramMap, registerFuture);
                }
                else if(config().getJsonObject("roleManagement").getBoolean("isOnTheFlyRoleCreationAllowed")) {
                    paramMap.put("roleNames", database.jsonArrayToParameterArray(userRoles));
                    paramMap.put("timestamp", timestamp);
                    paramMap.put("origin", message.origin());
                    database.query(AQL_registerUserWithEmailAuthOTFRoleCreation, paramMap, registerFuture);
                }
                else {
                    message.fail("Role management is not implemented yet");
                }
            });
            Map<String, Object> paramMap = new HashMap<>();
            paramMap.put("emailAddress", emailAddress);
            database.query(AQL_checkIfEmailIsAlreadyUsedForAuth, paramMap, checkFuture);
        }
        else {
            logger.warn(message.correlationID(), message.origin(), 501, "Unknown authentication type : " + authenticationType);
            message.fail(501, "AuthType is not known/supported.");
            return;
        }
    }

    @Override
    public void prepare(Future<Object> prepareFuture) {
        if(!config().containsKey("isUserRegistrationAllowed")) {
            config().put("isUserRegistrationAllowed", false);
            logger.settings("isUserRegistrationAllowed", false);
        }

        JsonObject userRegistration = config().getJsonObject("userRegistration");
        if(userRegistration == null) {
            userRegistration = new JsonObject();
            config().put("userRegistration", userRegistration);
            logger.settings("userRegistration", userRegistration);
        }
        if(!userRegistration.containsKey("whitelistByServiceOrigin")) {
            logger.settings("userRegistration.whitelistByServiceOrigin", "[]");
            userRegistration.put("whitelistByServiceOrigin", new JsonArray());
        }
        if(!userRegistration.containsKey("isEMailBasedRegistrationEnabled")) {
            logger.settings("userRegistration.isEMailBasedRegistrationEnabled", false);
            userRegistration.put("isEMailBasedRegistrationEnabled", false);
        }
        JsonObject email = userRegistration.getJsonObject("email");
        if(email == null) {
            email = new JsonObject();
            userRegistration.put("email", email);
            logger.settings("userRegistration.email", email);
        }
        String emailWhitelistPattern = email.getString("emailWhitelistPattern");
        if(emailWhitelistPattern == null || emailWhitelistPattern.isEmpty()) {
            emailWhitelistPattern = "^[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})$";
            email.put("emailWhitelistPattern", emailWhitelistPattern);
            logger.settings("userRegistration.email.emailWhitelistPattern", emailWhitelistPattern);
        }
        if(!email.containsKey("isEMailVerificationRequired")) {
            email.put("isEMailVerificationRequired", true);
            logger.settings("userRegistration.email.isEMailVerificationRequired", true);
        }

        JsonObject roleManagement = config().getJsonObject("roleManagement");
        if(roleManagement == null) {
            roleManagement = new JsonObject();
            config().put("roleManagement", roleManagement);
            logger.settings("roleManagement", roleManagement);
        }
        if(!roleManagement.containsKey("whitelistByServiceOrigin")) {
            roleManagement.put("whitelistByServiceOrigin", new JsonArray());
            logger.settings("roleManagement.whitelistByServiceOrigin", "[]");
        }
        if(!roleManagement.containsKey("whitelistByUserRole")) {
            roleManagement.put("whitelistByUserRole", new JsonArray());
            logger.settings("roleManagement.whitelistByUserRole", "[]");
        }
        if(!roleManagement.containsKey("isOnTheFlyRoleCreationAllowed")) {
            roleManagement.put("isOnTheFlyRoleCreationAllowed", false);
            logger.settings("roleManagement.isOnTheFlyRoleCreationAllowed", false);
        }

        JsonObject hashPolicy = config().getJsonObject("hashPolicy");
        if(hashPolicy == null) {
            hashPolicy = new JsonObject();
            config().put("hashPolicy", hashPolicy);
            logger.settings("hashPolicy", hashPolicy);
        }
        if(!hashPolicy.containsKey("minimumIterations")) {
            hashPolicy.put("minimumIterations", 10);
            logger.settings("hashPolicy.minimumIterations", 10);
        }
        if(!hashPolicy.containsKey("minimumLength")) {
            hashPolicy.put("minimumLength", 256);
            logger.settings("hashPolicy.minimumLength", 256);
        }

        emailPattern = Pattern.compile(emailWhitelistPattern, Pattern.CASE_INSENSITIVE);

        prepareFuture.complete();
    }

    @Override
    public void startConsuming() {

    }

    @Override
    public void shutdown(Future<Object> shutdownFuture) {
        shutdownFuture.complete();
    }
}
